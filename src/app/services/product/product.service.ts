import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private readonly httpClient: HttpClient) {

  }

  getProduct(size: Number) {
    return this.httpClient.get(`https://dummyjson.com/products?limit=${size}`);
  }

  addProduct(body: any) {
    return this.httpClient.post('https://dummyjson.com/products/add', body);
  }
}
