import { Component } from '@angular/core';
import { zip } from 'rxjs';
import { ProductService } from 'src/app/services/product/product.service';
import { AddProduct, Product } from '../model/model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  productList: Product[] = [];
  toggleMenu = false;

  constructor(private productService: ProductService) {

  }

  ngOnInit() {
    this.fetchAllProduct();
    this.addNewProduct();
  }

  fetchAllProduct() {
    const size = 10;
    this.productService.getProduct(size).subscribe((res: any) => {
      this.productList = res.products;
    });
  }

  addNewProduct() {
    const reqBody: AddProduct = {
      title: 'test-add-product aja nih',
      description: 'test description'
    }
    this.productService.addProduct(reqBody).subscribe(res => {
      console.log("res ", res);
    });
  }
}
